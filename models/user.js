const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    require: true
  },
  lastName: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  mobileNo: {
    type: String,
    require: true
  },
  income: [{
    category:{
      type:String,
      require: true,
    },
      value:[{
        total:{
          type: Number,
          require: true
        }
      }]
    
  }]
  ,
  expense: [{
    category:{
      type:String,
      require: true,
    },
      value:[{
        total:{
          type: Number,
          require: true
        }
      }]
    
  }]
})

module.exports = mongoose.model('User', userSchema)