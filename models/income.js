const mongoose = require('mongoose');
const IncomeSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  incomeCategory: {
    type: String,
    require: true
  },
  incomeEntry: {
    type: String,
    require: true
  },
  subTotal: {
    type: Number,
    default: 0
  },
  createdOn: {
    type: Date,
    default: new Date()
  }
})
module.exports = mongoose.model('Income', IncomeSchema)