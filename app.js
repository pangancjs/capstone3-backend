const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const cors = require('cors')

require('dotenv').config()
require('./db');

const app = express()
const port = process.env.PORT || 4000

app.use(helmet())
app.use(cors())
app.use(express.json())
app.use(morgan('dev'))

app.use('/api/users', require('./routes/userRoutes'))

app.use((err,req,res,next)=>{
  if( res.statusCode == 200) res.status(400)
  res.send({
      error: {
           message: err.message
      }
  })
})

app.listen( port ,()=>{
  console.log(`Server running on port ${port}`)
})