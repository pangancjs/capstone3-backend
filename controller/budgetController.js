const User = require('../models/user')
module.exports.addIncome = (req, res) => {

  User.findById(req.user.id, { password: 0 })
  .then(user =>{
    user.income.push({
      category: req.body.category
    })
    return user.save()
  })
    .then(user =>
      res.send(user))
    .catch(err => {
      res.send(err.message)
    })

}
module.exports.addIncomeEntry = (req, res) => {
  User.findById(req.user.id, { password: 0 })
  .then(user =>{
    let stor = user.income
   const result = stor.find(({ id }) => id === req.params.id);
   result.value.push({
     total: req.body.total
   })
     return user.save()
   })
 
   
    .then(user =>
      res.send(user))
    .catch(err => {
      res.send(err.message)
    })

}
module.exports.addExpense = (req, res) => {

  User.findById(req.user.id, { password: 0 })
  .then(user =>{
    user.expense.push({
      category: req.body.category
    })
    return user.save()
  })
    .then(user =>
      res.send(user))
    .catch(err => {
      res.send(err.message)
    })

}
module.exports.addExpenseEntry = (req, res) => {
  User.findById(req.user.id, { password: 0 })
  .then(user =>{
    let stor = user.expense
   const result = stor.find(({ id }) => id === req.params.id);
   result.value.push({
     total: req.body.total
   })
     return user.save()
   })
 
   
    .then(user =>
      res.send(user))
    .catch(err => {
      res.send(err.message)
    })

}

module.exports.UpdateIncome = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.income
      const result = stor.find(({ id }) => id === req.params.id);
      result.category = req.body.category
      user.save()
      res.send(result)
    })

}
module.exports.UpdateIncomeEntry = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.income
      const result = stor.find(({ id }) => id === req.params.id);
      let valTotal = result.value
      const total = valTotal.find(({ id }) => id === req.params.valId);
      total.total = req.body.total
      user.save()
      res.send(total)
    })

}
module.exports.UpdateExpense = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.expense
      const result = stor.find(({ id }) => id === req.params.id);
      result.category = req.body.category
      user.save()
      res.send(result)
    })

}
module.exports.UpdateExpenseEntry = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.expense
      const result = stor.find(({ id }) => id === req.params.id);
      let valTotal = result.value
      const total = valTotal.find(({ id }) => id === req.params.valId);
      total.total = req.body.total
      user.save()
      res.send(total)
    })

}
module.exports.DeleteIncome = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.income
      const arr = stor.filter((item) => item.id !== req.params.id);
      user.income = arr
      user.save()
      res.send(arr)
    })
}
module.exports.DeleteIncomeEntry = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.income
      const result = stor.find(({ id }) => id === req.params.id);
      let valTotal = result.value
      const arr = valTotal.filter((item ) => item.id !== req.params.valId);
      result.value = arr
      user.save()
      res.send(arr)
    })

}
module.exports.DeleteExpense = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.expense
      const arr = stor.filter((item) => item.id !== req.params.id);
      user.expense = arr
      user.save()
      res.send(arr)
    })
}
module.exports.DeleteExpenseEntry = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      let stor = user.expense
      const result = stor.find(({ id }) => id === req.params.id);
      let valTotal = result.value
      const arr = valTotal.filter((item ) => item.id !== req.params.valId);
      result.value = arr
      user.save()
      res.send(arr)
    })

}

module.exports.GetAllIncome = (req,res) =>{
  User.findById(req.user.id, { password: 0 })
    .then(props => {
      res.send(props.income)
    })
}
module.exports.GetAllExpense = (req,res) =>{
  User.findById(req.user.id, { password: 0 })
    .then(props => {
      res.send(props.expense)
    })
}