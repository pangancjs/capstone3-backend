const bcrypt = require('bcrypt')
const User = require ('./../models/user')
const { createToken } = require('./../Authentication')
const { json } = require('express')

module.exports.Register = (req, res) => {
  if (req.body.password !== req.body.confirmPassword) return res.send(false)
  const hash = bcrypt.hashSync(req.body.password, 9);
  req.body.password = hash;
  let newRegister = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password,
    isAdmin: req.body.isAdmin,
    mobileNo: req.body.mobileNo
  })
  newRegister.save()
    .then(() => res.send(true))
    .catch(err => res.send(err))

}
module.exports.Login = (req, res, next) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        res.status(401)
        next(new Error("Invalid credentials"))
      }
      else {
        let matchPass = bcrypt.compareSync(req.body.password, user.password);
        if (!matchPass) {
            res.status(401)
            next(new Error("Invalid credentials"))
        } else {
          res.send({ access: createToken(user) })
        }
      }
    }).catch(err => {
      console.log("catch", err)
    })
}
module.exports.getBudget = (req, res) =>{
  
  User.findById(req.params.id)
  .then(user =>{
    res.send({income: user.income , expense: user.expense})
  }).catch(err => res.send(err.message))
}
module.exports.GetUser = (req, res) => {
  User.findById(req.user.id, { password: 0 })
    .then(user => {
      res.send(user)
    })
}
module.exports.getTotal = (req, res) =>{
 
  User.findById(req.user.id)
  .then(user => {
    let subSum = user.income.map(data=> data.value.map(tots=>tots.total))
    let total = subSum.flat().reduce((a,b) => a+b)
    res.send({Total: total})
  })
}