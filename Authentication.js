const jwt =  require('jsonwebtoken')
const secret = "ecommerceSystem"

module.exports.createToken = (user) =>{
  let payload ={
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  return jwt.sign(payload, secret)
}
module.exports.verify = (req,res,next) =>{
  let token = req.headers.authorization;
  if (typeof token == "undefined") {
    res.send({ auth: "failed" })
  }
  else{
    token = token.slice(7,token.length)
    console.log(token)

    jwt.verify(token, secret, function(err, decoded) {
      if(err){
        res.send({auth: "failed"})
      }
      else{
        req.user = decoded
        next()
      }
    });
  }
}
module.exports.verifyAdmin = (req,res,next) => {
  if (req.user.isAdmin){
    next()
  }else{
    res.send(false)
  }
  }