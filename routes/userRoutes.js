const express =  require('express')
const {verify} = require('./../Authentication')
const {verifyAdmin} = require('./../Authentication')
const router = express.Router()
const {
  Register,
  Login,
  getBudget,
  GetUser,
  getTotal
} =  require('./../controller/userController')
const {
  addIncome,
  addIncomeEntry,
  addExpense,
  addExpenseEntry,
  UpdateIncome,
  UpdateIncomeEntry,
  UpdateExpense,
  UpdateExpenseEntry,
  DeleteIncome,
  DeleteIncomeEntry,
  DeleteExpense,
  DeleteExpenseEntry,
  GetAllIncome,
  GetAllExpense
} =  require('../controller/budgetController')
router.post('/', Register)
router.post('/login', Login)
router.post('/addIncome',verify,addIncome)
router.post('/addIncomeEntry/:id',verify,addIncomeEntry)
router.post('/addExpense',verify,addExpense)
router.post('/addExpenseEntry/:id',verify,addExpenseEntry)
router.put('/updateIncome/:id',verify,UpdateIncome)
router.put('/updateIncomeEntry/:id/:valId',verify,UpdateIncomeEntry)
router.put('/updateExpense/:id',verify,UpdateExpense)
router.put('/updateExpenseEntry/:id/:valId',verify,UpdateExpenseEntry)
router.delete('/DeleteExpense/:id',verify,DeleteExpense)
router.delete('/deleteExpenseEntry/:id/:valId',verify,DeleteExpenseEntry)
router.delete('/DeleteIncome/:id',verify,DeleteIncome)
router.delete('/deleteIncomeEntry/:id/:valId',verify,DeleteIncomeEntry)
router.get('/getTotal',verify,getTotal)
router.get('/getAllIncome',verify,GetAllIncome)
router.get('/getAllExpense',verify,GetAllExpense)
router.get('/getbudget/:id',verify,verifyAdmin,getBudget)
router.get('/',verify,GetUser)
module.exports = router